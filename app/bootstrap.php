<?php

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

//setcookie('nette-debug', 'AjA2PEuaSGCR6yBdKN8jPUdUX6jT4Vzg', strtotime('10 years'), '/', '', '', true);
$configurator->setDebugMode('AjA2PEuaSGCR6yBdKN8jPUdUX6jT4Vzg@172.19.0.1'); // enable for your remote IP
$configurator->enableTracy(__DIR__ . '/../log');

$configurator->setTimeZone('Europe/Prague');
$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

$container = $configurator->createContainer();

return $container;
